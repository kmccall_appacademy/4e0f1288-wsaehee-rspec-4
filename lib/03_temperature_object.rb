class Temperature
  def initialize(options = {})
    @options = options
    @fahrenheit = options[:f]
    @celsius = options[:c]
  end

  def self.from_celsius(temp)
    self.new(:c => temp)
  end

  def self.from_fahrenheit(temp)
    self.new(:f => temp)
  end

  def fahrenheit=(temp)
    @fahrenheit = temp
  end

  def celsius=(temp)
    @celsius = temp
  end

  def in_fahrenheit
    @fahrenheit == nil ? ((@celsius * 9)/5.0) + 32 : @fahrenheit
  end

  def in_celsius
    @celsius == nil ? ((@fahrenheit - 32) * 5)/9.0 : @celsius
  end
end

class Celsius < Temperature
  def initialize(temp)
    self.celsius = temp
  end
end

class Fahrenheit < Temperature
  def initialize(temp)
    self.fahrenheit = temp
  end
end
