class Book
  attr_reader :title

  def title=(title)
    title_array = title.split(" ")
    title_array.collect do |word|
      unless is_conjunction?(word) || is_preposition?(word) || is_article?(word)
        word.capitalize!
      end
    end
    title_array[0].capitalize!
    @title = title_array.join(" ")
  end

  def is_conjunction?(word)
    conjunctions = ["and", "as", "before", "neither", "just as", "but", "yet", "for", "nor", "so", "or"]
    conjunctions.any?{|conjunction| conjunction == word}
  end

  def is_preposition?(word)
    prepositions = ["of", "in", "to", "for", "with", "on", "at", "from", "by", "about"]
    prepositions.any?{|preposition| preposition == word}
  end

  def is_article?(word)
    articles = ["a", "an", "the"]
    articles.any?{|article| article == word}
  end

end
