class Dictionary
  def initialize
    @dictionary = {}
  end

  def entries
    @dictionary
  end

  def add(entry)
    entry.class == Hash ? @dictionary.merge!(entry) : @dictionary[entry] = nil
    @dictionary
  end

  def include?(keyword)
    @dictionary.has_key?(keyword)
  end

  def find(prefix)
    matches = {}
    regexp = Regexp.new(prefix)
    @dictionary.each do |word, definition|
      matches[word] = definition if regexp.match(word)
    end
    matches
  end

  def keywords
    @dictionary.map do |word, defintion|
      word
    end.sort
  end

  def printable
    sorted_words = self.keywords
    print_list = ""
    sorted_words.each do |word|
      print_list.concat("[#{word}] \"#{@dictionary[word]}\"\n")
    end
    print_list.chomp
  end
end
