class Timer
  attr_reader :seconds

  def initialize
    @seconds = 0
  end

  def seconds=(seconds)
    @seconds = seconds
  end

  def time_string
    hour = (seconds/60/60).floor
    minute = (seconds/60).floor - (hour * 60)
    @seconds = seconds % 60
    @seconds = "#{add_zero(hour)}:#{add_zero(minute)}:#{add_zero(seconds)}"
  end

  def add_zero(time)
    time < 10 ? "0#{time.to_s}" : time
  end
end
